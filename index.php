<!DOCTYPE html>
<html>
<head>
	<title>Index Page | Show All Data</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

</head>
<body style="margin-left: 40px">
<h1>Data Mahasiswa</h1>
<hr/>
<a href="tambah.php" class="btn">Tambah Mahasiswa</a>
<br/>
<table class="table table-hover" style="width: 80%">
	<thead>
		<th style="width: 15%">NPM</th>
		<th style="width: 30%">Nama</th>
		<th>Alamat</th>
		<th style="text-align:center; width:20%">Action</th>
	</thead>
	<?php 
	include("connection.php");
	$query = "SELECT * FROM mahasiswa";
	$result = mysql_query($query);
	while ($row = mysql_fetch_array($result)) { 
	?>
	<tbody>
		<td><?php echo $row["npm"]; ?></td>
		<td><?php echo $row["nama"]; ?></td>
		<td><?php echo $row["alamat"]; ?></td>
		<td style="text-align:center">
		<a href="edit.php?npm=<?php echo $row["npm"]; ?>">Edit</a>  |  
		<a href="delete.php?npm=<?php echo $row["npm"]; ?>">Delete</a>
		</th>
	</tbody>
	<?php } ?>
	<tfoot>
		<th style="width: 15%">NPM</th>
		<th style="width: 30%">Nama</th>
		<th>Alamat</th>
		<th style="text-align:center; width:20%">Action</th>
	</tfoot>
</table>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>