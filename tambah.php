<!DOCTYPE html>
<html>
<head>
	<title>Tambah Mahasiswa</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

</head>
<body style="margin-left: 20px; margin-top: 20px">

<h1>Tambah Data Mahasiswa</h1>
<hr/>

<form action="proses.php" method="POST" style="width: 40%">
	<div class="form-group">
		<label>NPM</label>
		<input type="text" class="form-control" name="npm" placeholder="Masukan NPM Anda">
	</div>
	<div class="form-group">
		<label>Nama</label>
		<input type="text" class="form-control" name="nama" placeholder="Masukan Nama Anda">
	</div>
	<div class="form-group">
		<label>Alamat</label>
		<input type="text" class="form-control" name="alamat" placeholder="Masukan Alamat Anda">
	</div>
	<button type="submit" class="btn btn-success">Submit</button>
</form>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>